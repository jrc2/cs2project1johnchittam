package edu.westga.cs1302.menureview.test.menu;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.menureview.model.Menu;
import edu.westga.cs1302.menureview.model.MenuItem;
import edu.westga.cs1302.menureview.test.TestingConstants;

class TestFindPriceOfMostExpensiveItem {

	@Test
	void testEmptyMenu() {
		Menu menu = new Menu();
		double price = menu.findPriceOfMostExpensiveItem();
		assertTrue(price < 0);
	}
	
	@Test
	void testOneItemMenu() {
		Menu menu = new Menu();
		menu.add(new MenuItem("Butter Pecan Fudge", "Rich and nutty fudge", 3.99, 940, 9));
		double price = menu.findPriceOfMostExpensiveItem();
		assertEquals(3.99, price, TestingConstants.DELTA);
	}
	
	@Test
	void testMultiItemsMostExpensiveFirst() {
		Menu menu = new Menu();
		menu.add(new MenuItem("Grilled Top Sirloin", "8 oz. USDA Select top sirloin", 15.99, 1240, 8));
		menu.add(new MenuItem("Butter Pecan Fudge", "Rich and nutty fudge", 3.99, 940, 9));
		menu.add(new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 10.99, 1240, 7));
		double price = menu.findPriceOfMostExpensiveItem();
		assertEquals(15.99, price, TestingConstants.DELTA);
	}
	
	@Test
	void testMultiItemsMostExpensiveMiddle() {
		Menu menu = new Menu();
		menu.add(new MenuItem("Butter Pecan Fudge", "Rich and nutty fudge", 3.99, 940, 9));
		menu.add(new MenuItem("Grilled Top Sirloin", "8 oz. USDA Select top sirloin", 15.99, 1240, 8));
		menu.add(new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 10.99, 1240, 7));
		double price = menu.findPriceOfMostExpensiveItem();
		assertEquals(15.99, price, TestingConstants.DELTA);
	}
	
	@Test
	void testMultiItemsMostExpensiveLast() {
		Menu menu = new Menu();
		menu.add(new MenuItem("Butter Pecan Fudge", "Rich and nutty fudge", 3.99, 940, 9));
		menu.add(new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 10.99, 1240, 7));
		menu.add(new MenuItem("Grilled Top Sirloin", "8 oz. USDA Select top sirloin", 15.99, 1240, 8));
		double price = menu.findPriceOfMostExpensiveItem();
		assertEquals(15.99, price, TestingConstants.DELTA);
	}
}
