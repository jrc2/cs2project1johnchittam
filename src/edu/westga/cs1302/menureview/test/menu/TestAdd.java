package edu.westga.cs1302.menureview.test.menu;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.menureview.model.Menu;
import edu.westga.cs1302.menureview.model.MenuItem;

class TestAdd {

	@Test
	void testAddNullMenuItem() {
		Menu menu = new Menu();
		assertThrows(IllegalArgumentException.class, () -> menu.add(null));
	}
	
	@Test
	void testAddMenuItemToEmptyMenu() {
		Menu menu = new Menu();
		menu.add(new MenuItem("Butter Pecan Fudge", "Rich and nutty fudge", 3.99, 940, 9));
		assertEquals(1, menu.size());
	}
	
	@Test
	void testAddMultiplesMenuItemsToMenu() {
		Menu menu = new Menu();
		menu.add(new MenuItem("Butter Pecan Fudge", "Rich and nutty fudge", 3.99, 940, 9));
		menu.add(new MenuItem("Caramel Pie", "Caramel in a graham cracker crust", 3.49, 620, 5));
		assertEquals(2, menu.size());
	}
	
	@Test
	void testAddDuplicateItem() {
		Menu menu = new Menu();
		menu.add(new MenuItem("Butter Pecan Fudge", "Rich and nutty fudge", 3.99, 940, 9));
		menu.add(new MenuItem("Caramel Pie", "Caramel in a graham cracker crust", 3.49, 620, 5));
		
		assertAll(
			()-> assertFalse(menu.add(new MenuItem("   carAMEL pIe ", "test", 1.23, 456, 7))),
			()-> assertEquals(2, menu.size())
		);
	}

}
