package edu.westga.cs1302.menureview.test.menu;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.menureview.model.Menu;

class TestConstruction {
	@Test
	void testDefaultMenu() {
		Menu menu = new Menu();

		assertAll(() -> assertEquals(0, menu.size()));
	}
}
