package edu.westga.cs1302.menureview.test.menu;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.menureview.model.Menu;
import edu.westga.cs1302.menureview.model.MenuItem;

class TestFindNameOfBestMenuItem {

	@Test
	void testEmptyMenu() {
		Menu menu = new Menu();
		String name = menu.findNameOfBestMenuItem();
		assertEquals(null, name);
	}
	
	@Test
	void testOneItemMenu() {
		Menu menu = new Menu();
		menu.add(new MenuItem("Butter Pecan Fudge", "Rich and nutty fudge", 3.99, 940, 9));
		String name = menu.findNameOfBestMenuItem();
		assertEquals("Butter Pecan Fudge", name);
	}
	
	@Test
	void testMultiItemsBestItemFirst() {
		Menu menu = new Menu();
		menu.add(new MenuItem("Butter Pecan Fudge", "Rich and nutty fudge", 3.99, 940, 9));
		menu.add(new MenuItem("Grilled Top Sirloin", "8 oz. USDA Select top sirloin", 15.99, 1240, 8));
		menu.add(new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 10.99, 1240, 7));
		String name = menu.findNameOfBestMenuItem();
		assertEquals("Butter Pecan Fudge", name);
	}
	
	@Test
	void testMultiItemsBestItemMiddle() {
		Menu menu = new Menu();
		menu.add(new MenuItem("Grilled Top Sirloin", "8 oz. USDA Select top sirloin", 15.99, 1240, 8));
		menu.add(new MenuItem("Butter Pecan Fudge", "Rich and nutty fudge", 3.99, 940, 9));
		menu.add(new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 10.99, 1240, 7));
		String name = menu.findNameOfBestMenuItem();
		assertEquals("Butter Pecan Fudge", name);
	}
	
	@Test
	void testMultiItemsBestItemLast() {
		Menu menu = new Menu();
		menu.add(new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 10.99, 1240, 7));
		menu.add(new MenuItem("Grilled Top Sirloin", "8 oz. USDA Select top sirloin", 15.99, 1240, 8));
		menu.add(new MenuItem("Butter Pecan Fudge", "Rich and nutty fudge", 3.99, 940, 9));
		String name = menu.findNameOfBestMenuItem();
		assertEquals("Butter Pecan Fudge", name);
	}
}
