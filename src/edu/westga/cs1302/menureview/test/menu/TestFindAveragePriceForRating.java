package edu.westga.cs1302.menureview.test.menu;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.menureview.model.Menu;
import edu.westga.cs1302.menureview.model.MenuItem;
import edu.westga.cs1302.menureview.test.TestingConstants;

class TestFindAveragePriceForRating {
	@Test
	void testInvalidRating() {
		Menu menu = new Menu();
		assertThrows(IllegalArgumentException.class, () -> menu.findAveragePriceForRating(0));
	}
	
	@Test
	void testEmptyMenu() {
		Menu menu = new Menu();
		double price = menu.findAveragePriceForRating(9);
		assertEquals(0, price);
	}
	
	@Test
	void testOneItemMenuWithoutSpecifiedRating() {
		Menu menu = new Menu();
		menu.add(new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 10.99, 1240, 7));
		double price = menu.findAveragePriceForRating(9);
		assertEquals(0, price, TestingConstants.DELTA);
	}
	
	@Test
	void testMultiItemMenuWithoutSpecifiedRating() {
		Menu menu = new Menu();
		menu.add(new MenuItem("Breadstick", "Breadstick with garlic butter", 0.99, 190, 6));
		menu.add(new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 10.99, 1240, 7));
		double price = menu.findAveragePriceForRating(9);
		assertEquals(0, price, TestingConstants.DELTA);
	}
	
	@Test
	void testOneItemMenuWithSpecifiedRating() {
		Menu menu = new Menu();
		menu.add(new MenuItem("Butter Pecan Fudge", "Rich and nutty fudge", 3.99, 940, 9));
		double price = menu.findAveragePriceForRating(9);
		assertEquals(3.99, price, TestingConstants.DELTA);
	}
	
	@Test
	void testMultiItemMenuOneWithSpecifiedRating() {
		Menu menu = new Menu();
		menu.add(new MenuItem("Breadstick", "Breadstick with garlic butter", 0.99, 190, 6));
		menu.add(new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 10.99, 1240, 7));
		menu.add(new MenuItem("Butter Pecan Fudge", "Rich and nutty fudge", 3.99, 940, 9));
		double price = menu.findAveragePriceForRating(9);
		assertEquals(3.99, price, TestingConstants.DELTA);
	}
	
	@Test
	void testMultiItemMenuSomeWithSpecifiedRating() {
		Menu menu = new Menu();
		menu.add(new MenuItem("Breadstick", "Breadstick with garlic butter", 0.99, 190, 7));
		menu.add(new MenuItem("Caramel Pie", "Caramel in a graham cracker crust", 3.49, 620, 9));
		menu.add(new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 10.99, 1240, 7));
		menu.add(new MenuItem("Butter Pecan Fudge", "Rich and nutty fudge", 3.99, 940, 9));
		double price = menu.findAveragePriceForRating(9);
		assertEquals(3.74, price, TestingConstants.DELTA);
	}

	@Test
	void testMultiItemMenuAllWithSpecifiedRating() {
		Menu menu = new Menu();
		menu.add(new MenuItem("Caramel Pie", "Caramel in a graham cracker crust", 3.49, 620, 9));
		menu.add(new MenuItem("Caesar Salad", "Crisp romaine with grilled chicken", 9.99, 770, 9));
		menu.add(new MenuItem("Butter Pecan Fudge", "Rich and nutty fudge", 3.99, 940, 9));
		double price = menu.findAveragePriceForRating(9);
		assertEquals(5.823333, price, TestingConstants.DELTA);
	}
}
