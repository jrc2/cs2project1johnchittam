package edu.westga.cs1302.menureview.test.menu;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.menureview.model.Menu;
import edu.westga.cs1302.menureview.model.MenuItem;

class TestFindBestMenuItem {
	@Test
	void testEmptyMenu() {
		Menu menu = new Menu();
		MenuItem resultItem = menu.findBestMenuItem();
		assertEquals(null, resultItem);
	}
	
	@Test
	void testOneItemMenu() {
		Menu menu = new Menu();
		MenuItem itemA = new MenuItem("Butter Pecan Fudge", "Rich and nutty fudge", 3.99, 940, 9);
		menu.add(itemA);
		MenuItem resultItem = menu.findBestMenuItem();
		assertEquals(itemA, resultItem);
	}
	
	@Test
	void testMultiItemsBestFirst() {
		Menu menu = new Menu();
		MenuItem itemA = new MenuItem("Butter Pecan Fudge", "Rich and nutty fudge", 3.99, 940, 9);
		MenuItem itemB = new MenuItem("Grilled Top Sirloin", "8 oz. USDA Select top sirloin", 15.99, 1240, 8);
		MenuItem itemC = new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 10.99, 1240, 7);
		menu.add(itemA);
		menu.add(itemB);
		menu.add(itemC);
		MenuItem resultItem = menu.findBestMenuItem();
		assertEquals(itemA, resultItem);
	}
	
	@Test
	void testMultiItemsBestMiddle() {
		Menu menu = new Menu();
		MenuItem itemA = new MenuItem("Butter Pecan Fudge", "Rich and nutty fudge", 3.99, 940, 9);
		MenuItem itemB = new MenuItem("Grilled Top Sirloin", "8 oz. USDA Select top sirloin", 15.99, 1240, 8);
		MenuItem itemC = new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 10.99, 1240, 7);
		menu.add(itemB);
		menu.add(itemA);
		menu.add(itemC);
		MenuItem resultItem = menu.findBestMenuItem();
		assertEquals(itemA, resultItem);
	}
	
	@Test
	void testMultiItemsBestLast() {
		Menu menu = new Menu();
		MenuItem itemA = new MenuItem("Butter Pecan Fudge", "Rich and nutty fudge", 3.99, 940, 9);
		MenuItem itemB = new MenuItem("Grilled Top Sirloin", "8 oz. USDA Select top sirloin", 15.99, 1240, 8);
		MenuItem itemC = new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 10.99, 1240, 7);
		menu.add(itemB);
		menu.add(itemC);
		menu.add(itemA);
		MenuItem resultItem = menu.findBestMenuItem();
		assertEquals(itemA, resultItem);
	}
}
