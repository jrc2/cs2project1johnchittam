package edu.westga.cs1302.menureview.test.menu;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.menureview.model.Menu;
import edu.westga.cs1302.menureview.model.MenuItem;

class TestSearchNameOrDescription {

	@Test
	void testNullQuery() {
		Menu myMenu = new Menu();
		
		assertThrows(IllegalArgumentException.class, ()->
			myMenu.filterItems(null, 1, MenuItem.UPPER_BOUND_RATING)
		);
	}
	
	@Test
	void testEmptyQuery() {
		Menu myMenu = new Menu();
		
		myMenu.add(new MenuItem("one", "first item", 1.99, 200, 8));
		myMenu.add(new MenuItem("two", "second item", 1.99, 200, 8));
		myMenu.add(new MenuItem("three", "third item", 1.99, 200, 8));
		
		assertEquals(myMenu.getMenuItems(), myMenu.filterItems("", 1, MenuItem.UPPER_BOUND_RATING));
	}
	
	@Test
	void testSearchQueryIn1stName() {
		Menu myMenu = new Menu();
		
		myMenu.add(new MenuItem("one", "first item", 1.99, 200, 8));
		myMenu.add(new MenuItem("two", "second item", 1.99, 200, 8));
		myMenu.add(new MenuItem("three", "third item", 1.99, 200, 8));
		
		ArrayList<MenuItem> expectedResults = new ArrayList<MenuItem>();
		expectedResults.add(myMenu.getMenuItems().get(0));
				
		assertEquals(expectedResults.toString(), myMenu.filterItems("one", 1, MenuItem.UPPER_BOUND_RATING).toString());
	}
	
	@Test
	void testSearchQueryIn2ndItemDescription() {
		Menu myMenu = new Menu();
		
		myMenu.add(new MenuItem("one", "first item", 1.99, 200, 8));
		myMenu.add(new MenuItem("two", "second item", 1.99, 200, 8));
		myMenu.add(new MenuItem("three", "third item", 1.99, 200, 8));
		
		ArrayList<MenuItem> expectedResults = new ArrayList<MenuItem>();
		expectedResults.add(myMenu.getMenuItems().get(1));
				
		assertEquals(expectedResults.toString(), myMenu.filterItems("secon", 1, MenuItem.UPPER_BOUND_RATING).toString());
	}
	
	@Test
	void testSearchQueryInLastItem() {
		Menu myMenu = new Menu();
		
		myMenu.add(new MenuItem("one", "first item", 1.99, 200, 8));
		myMenu.add(new MenuItem("two", "second item", 1.99, 200, 8));
		myMenu.add(new MenuItem("three", "third item", 1.99, 200, 8));
		
		ArrayList<MenuItem> expectedResults = new ArrayList<MenuItem>();
		expectedResults.add(myMenu.getMenuItems().get(2));
				
		assertEquals(expectedResults.toString(), myMenu.filterItems("three", 1, MenuItem.UPPER_BOUND_RATING).toString());
	}
	
	@Test
	void testTrimQuery() {
		Menu myMenu = new Menu();
		
		myMenu.add(new MenuItem("one", "first item", 1.99, 200, 8));
		myMenu.add(new MenuItem("two", "second item", 1.99, 200, 8));
		myMenu.add(new MenuItem("three", "third item", 1.99, 200, 8));
		
		ArrayList<MenuItem> expectedResults = new ArrayList<MenuItem>();
		expectedResults.add(myMenu.getMenuItems().get(1));
				
		assertEquals(expectedResults.toString(), 
			myMenu.filterItems("      two		", 1, MenuItem.UPPER_BOUND_RATING).toString()
		);
	}
	
	@Test
	void testMultipleMatchesInDescription() {
		Menu myMenu = new Menu();
		
		myMenu.add(new MenuItem("one", "first item", 1.99, 200, 8));
		myMenu.add(new MenuItem("two", "second item", 1.99, 200, 8));
		myMenu.add(new MenuItem("three", "third item", 1.99, 200, 8));
		
		ArrayList<MenuItem> expectedResults = new ArrayList<MenuItem>();
		expectedResults.add(myMenu.getMenuItems().get(0));
		expectedResults.add(myMenu.getMenuItems().get(1));
		expectedResults.add(myMenu.getMenuItems().get(2));
				
		assertEquals(expectedResults.toString(), 
			myMenu.filterItems(" tem	", 1, MenuItem.UPPER_BOUND_RATING).toString()
		);
	}
	
	@Test
	void testMultipleMatchesInName() {
		Menu myMenu = new Menu();
		
		myMenu.add(new MenuItem("one", "first item", 1.99, 200, 8));
		myMenu.add(new MenuItem("two", "second item", 1.99, 200, 8));
		myMenu.add(new MenuItem("three", "third item", 1.99, 200, 8));
		
		ArrayList<MenuItem> expectedResults = new ArrayList<MenuItem>();
		expectedResults.add(myMenu.getMenuItems().get(0));
		expectedResults.add(myMenu.getMenuItems().get(1));
				
		assertEquals(expectedResults.toString(), 
			myMenu.filterItems("o", 1, MenuItem.UPPER_BOUND_RATING).toString()
		);
	}
	
	@Test
	void testCaseSensitivityInDescription() {
		Menu myMenu = new Menu();
		
		myMenu.add(new MenuItem("one", "first item", 1.99, 200, 8));
		myMenu.add(new MenuItem("two", "second item", 1.99, 200, 8));
		myMenu.add(new MenuItem("three", "third item", 1.99, 200, 8));
		
		ArrayList<MenuItem> expectedResults = new ArrayList<MenuItem>();
		expectedResults.add(myMenu.getMenuItems().get(0));
		expectedResults.add(myMenu.getMenuItems().get(1));
		expectedResults.add(myMenu.getMenuItems().get(2));
				
		assertEquals(expectedResults.toString(), 
			myMenu.filterItems("ITeM", 1, MenuItem.UPPER_BOUND_RATING).toString()
		);
	}
	
	@Test
	void testCaseSensitivityInName() {
		Menu myMenu = new Menu();
		
		myMenu.add(new MenuItem("one", "first item", 1.99, 200, 8));
		myMenu.add(new MenuItem("wxy", "second item", 1.99, 200, 8));
		myMenu.add(new MenuItem("wxyz", "third item", 1.99, 200, 8));
		
		ArrayList<MenuItem> expectedResults = new ArrayList<MenuItem>();
		expectedResults.add(myMenu.getMenuItems().get(1));
		expectedResults.add(myMenu.getMenuItems().get(2));
				
		assertEquals(expectedResults.toString(), 
			myMenu.filterItems("WX", 1, MenuItem.UPPER_BOUND_RATING).toString()
		);
	}
	
	@Test
	void testNameAndDescriptionMatches() {
		Menu myMenu = new Menu();
		
		myMenu.add(new MenuItem("one", "FIRST item", 1.99, 200, 8));
		myMenu.add(new MenuItem("two", "second item", 1.99, 200, 8));
		myMenu.add(new MenuItem("three", "third item", 1.99, 200, 8));
		
		ArrayList<MenuItem> expectedResults = new ArrayList<MenuItem>();
		expectedResults.add(myMenu.getMenuItems().get(0));
		expectedResults.add(myMenu.getMenuItems().get(1));
		expectedResults.add(myMenu.getMenuItems().get(2));
				
		assertEquals(expectedResults.toString(), 
			myMenu.filterItems("T", 1, MenuItem.UPPER_BOUND_RATING).toString()
		);
	}

}
