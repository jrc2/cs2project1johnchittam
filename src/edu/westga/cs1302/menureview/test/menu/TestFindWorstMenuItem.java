package edu.westga.cs1302.menureview.test.menu;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.menureview.model.Menu;
import edu.westga.cs1302.menureview.model.MenuItem;

class TestFindWorstMenuItem {
	@Test
	void testEmptyMenu() {
		Menu menu = new Menu();
		MenuItem resultItem = menu.findWorstMenuItem();
		assertEquals(null, resultItem);
	}
	
	@Test
	void testOneItemMenu() {
		Menu menu = new Menu();
		MenuItem itemA = new MenuItem("Butter Pecan Fudge", "Rich and nutty fudge", 3.99, 940, 9);
		menu.add(itemA);
		MenuItem resultItem = menu.findWorstMenuItem();
		assertEquals(itemA, resultItem);
	}
	
	@Test
	void testMultiItemsWorstFirst() {
		Menu menu = new Menu();
		MenuItem itemA = new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 10.99, 1240, 7);
		MenuItem itemB = new MenuItem("Butter Pecan Fudge", "Rich and nutty fudge", 3.99, 940, 9);
		MenuItem itemC = new MenuItem("Grilled Top Sirloin", "8 oz. USDA Select top sirloin", 15.99, 1240, 8);	
		menu.add(itemA);
		menu.add(itemB);
		menu.add(itemC);
		MenuItem resultItem = menu.findWorstMenuItem();
		assertEquals(itemA, resultItem);
	}
	
	@Test
	void testMultiItemsWorstMiddle() {
		Menu menu = new Menu();
		MenuItem itemA = new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 10.99, 1240, 7);
		MenuItem itemB = new MenuItem("Butter Pecan Fudge", "Rich and nutty fudge", 3.99, 940, 9);
		MenuItem itemC = new MenuItem("Grilled Top Sirloin", "8 oz. USDA Select top sirloin", 15.99, 1240, 8);	
		menu.add(itemB);
		menu.add(itemA);
		menu.add(itemC);
		MenuItem resultItem = menu.findWorstMenuItem();
		assertEquals(itemA, resultItem);
	}
	
	@Test
	void testMultiItemsWorstLast() {
		Menu menu = new Menu();
		MenuItem itemA = new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 10.99, 1240, 7);
		MenuItem itemB = new MenuItem("Butter Pecan Fudge", "Rich and nutty fudge", 3.99, 940, 9);
		MenuItem itemC = new MenuItem("Grilled Top Sirloin", "8 oz. USDA Select top sirloin", 15.99, 1240, 8);	
		menu.add(itemB);
		menu.add(itemC);
		menu.add(itemA);
		MenuItem resultItem = menu.findWorstMenuItem();
		assertEquals(itemA, resultItem);
	}
}
