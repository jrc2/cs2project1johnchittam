package edu.westga.cs1302.menureview.test.menuitem;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.menureview.model.MenuItem;

class TestConstruction {
	private static final double DELTA = 0.000001;

	@Test
	void testValidConstruction() {
		MenuItem menuItem = new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 10.99, 1240, 7);
		assertAll(() -> assertEquals("Chicken Tenders", menuItem.getName()),
				() -> assertEquals("Crispy breaded chicken tenders", menuItem.getDescription()),
				() -> assertEquals(10.99, menuItem.getPrice(), DELTA), () -> assertEquals(1240, menuItem.getCalories()),
				() -> assertEquals(7, menuItem.getRating()));
	}

	@Test
	void testNullName() {
		IllegalArgumentException exc = assertThrows(IllegalArgumentException.class,
				() -> new MenuItem(null, "Crispy breaded chicken tenders", 10.99, 1240, 7));
		assertEquals("name cannot be null.", exc.getMessage());
	}

	@Test
	void testEmptyName() {
		assertThrows(IllegalArgumentException.class, () -> new MenuItem("", "Crispy breaded chicken tenders", 10.99, 1240, 7));
	}

	@Test
	void testNullDescription() {
		IllegalArgumentException exc = assertThrows(IllegalArgumentException.class, () -> new MenuItem("Chicken Tenders", null, 10.99, 1240, 7));
		assertEquals("description cannot be null.", exc.getMessage());
	}
	
	@Test
	void testEmptyDescription() {
		assertThrows(IllegalArgumentException.class, () -> new MenuItem("Chicken Tenders", "", 10.99, 1240, 7));
	}
	
	@Test
	void testPriceJustBelowLowerBound() {
		assertThrows(IllegalArgumentException.class,
				() -> new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", -0.01, 1240, 7));
	}

	@Test
	void testPriceAtLowerBound() {
		MenuItem menuItem = new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 0, 1240, 7);
		assertEquals(0, menuItem.getPrice(), DELTA);
	}

	@Test
	void testPriceJustAboveLowerBound() {
		MenuItem menuItem = new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 0.01, 1240, 7);
		assertEquals(0.01, menuItem.getPrice(), DELTA);
	}

	@Test
	void testCaloriesJustBelowLowerBound() {
		assertThrows(IllegalArgumentException.class,
				() -> new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 10.99, -1, 7));
	}

	@Test
	void testCaloriesAtLowerBound() {
		MenuItem menuItem = new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 10.99, 0, 7);
		assertEquals(0, menuItem.getCalories());
	}

	@Test
	void testCaloriesJustAboveLowerBound() {
		MenuItem menuItem = new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 10.99, 1, 7);
		assertEquals(1, menuItem.getCalories());
	}

	@Test
	void testRatingJustBelowLowerBound() {
		assertThrows(IllegalArgumentException.class,
				() -> new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 10.99, 1240, 0));
	}

	@Test
	void testRatingAtLowerBound() {
		MenuItem menuItem = new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 10.99, 1240, 1);
		assertEquals(1, menuItem.getRating());
	}

	@Test
	void testRatingJustAboveLowerBound() {
		MenuItem menuItem = new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 0.01, 1240, 2);
		assertEquals(2, menuItem.getRating());
	}

	@Test
	void testRatingJustAboveUpperBound() {
		assertThrows(IllegalArgumentException.class,
				() -> new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 10.99, 1240, 11));
	}

	@Test
	void testRatingAtUpperBound() {
		MenuItem menuItem = new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 10.99, 1240, 10);
		assertEquals(10, menuItem.getRating());
	}

	@Test
	void testRatingJustBelowUpperBound() {
		MenuItem menuItem = new MenuItem("Chicken Tenders", "Crispy breaded chicken tenders", 0.01, 1240, 9);
		assertEquals(9, menuItem.getRating());
	}
}
