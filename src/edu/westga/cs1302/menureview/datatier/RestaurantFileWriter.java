package edu.westga.cs1302.menureview.datatier;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

import edu.westga.cs1302.menureview.model.MenuItem;
import edu.westga.cs1302.menureview.model.Restaurant;
import edu.westga.cs1302.menureview.model.RestaurantGroup;
import edu.westga.cs1302.menureview.resources.UI;

/**
 * The Class RestaurantFileWriter.
 * 
 * @author CS1302
 */
public class RestaurantFileWriter {

	private File restaurantFile;

	/**
	 * Instantiates a new restaurant file writer.
	 *
	 * @precondition restaurantFile != null
	 * @postcondition none
	 * 
	 * @param restaurantFile the restaurant file
	 */
	public RestaurantFileWriter(File restaurantFile) {
		if (restaurantFile == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.RESTAURANT_FILE_CANNOT_BE_NULL);
		}

		this.restaurantFile = restaurantFile;
	}
	
	/**
	 * Writes all the menu items from all restaurants to the specified restaurant menu file. 
	 * Each menu item will be on a separate line and of the following format:
	 * restaurantName,itemName,description,calories,price,rating
	 * 
	 * @precondition restaurantGroup != null
	 * @postcondition none
	 * 
	 * @param restaurantGroup the group of restaurants from which to write the file
	 * @throws FileNotFoundException the file not found exception
	 */
	public void writeMenuItemsForAllRestaurants(RestaurantGroup restaurantGroup) throws FileNotFoundException {
		if (restaurantGroup == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.RESTAURANT_GROUP_CANNOT_BE_NULL);
		}
		
		String output = "";
		
		try (PrintWriter writer = new PrintWriter(this.restaurantFile)) {
			for (Restaurant currRestaurant : restaurantGroup.getRestaurants()) {
				ArrayList<MenuItem> menuItems = currRestaurant.getMenu().getMenuItems();
				output += this.writeOutputIncludingRestaurant(menuItems, currRestaurant.getName());
			}
			
			writer.print(output);
		}
		
	}
	
	private String writeOutputIncludingRestaurant(ArrayList<MenuItem> menuItems, String restaurantName) {
		String output = "";
		
		for (MenuItem currItem : menuItems) {
			output += restaurantName + RestaurantFileReader.FIELD_SEPARATOR
					+ currItem.getName() + RestaurantFileReader.FIELD_SEPARATOR
					+ currItem.getDescription() + RestaurantFileReader.FIELD_SEPARATOR
					+ currItem.getCalories() + RestaurantFileReader.FIELD_SEPARATOR
					+ currItem.getPrice() + RestaurantFileReader.FIELD_SEPARATOR
					+ currItem.getRating() + System.lineSeparator();
		}
		
		return output;
	}
	
//	/**
//	 * Writes all the menu items of the restaurant to the specified restaurant menu file. Each menu item
//	 * will be on a separate line and of the following format:
//	 * name,description,calories,price,rating
//	 * 
//	 * @precondition menu items != null
//	 * @postcondition none
//	 * 
//	 * @param menuItems the collection of menu items to write to file.
//	 * @throws FileNotFoundException the file not found exception
//	 */
//	public void write(ArrayList<MenuItem> menuItems) throws FileNotFoundException {
//		if (menuItems == null) {
//			throw new IllegalArgumentException(UI.ExceptionMessages.MENUITEMS_CANNOT_BE_NULL);
//		}
//
//		try (PrintWriter writer = new PrintWriter(this.restaurantFile)) {
//			for (MenuItem currItem : menuItems) {
//				String output = currItem.getName() + RestaurantFileReader.FIELD_SEPARATOR;
//				output += currItem.getDescription() + RestaurantFileReader.FIELD_SEPARATOR;
//				output += currItem.getCalories() + RestaurantFileReader.FIELD_SEPARATOR;
//				output += currItem.getPrice() + RestaurantFileReader.FIELD_SEPARATOR;
//				output += currItem.getRating();
//				
//				writer.println(output);
//			}
//		}
//
//	}

}
