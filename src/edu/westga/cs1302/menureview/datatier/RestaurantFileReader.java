package edu.westga.cs1302.menureview.datatier;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

import edu.westga.cs1302.menureview.model.MenuItem;
import edu.westga.cs1302.menureview.model.RestaurantGroup;
import edu.westga.cs1302.menureview.resources.UI;

/**
 * The Class RestaurantFileReader. 
 * 
 * Reads an .rmd (Restaurant Menu Data) File which is a CSV file with the following format:
 * name,description,calories,price,rating
 * 
 * @author CS1302
 */
public class RestaurantFileReader {
	public static final String FIELD_SEPARATOR = ",";

	private File restaurantFile;

	/**
	 * Instantiates a new restaurant file reader.
	 *
	 * @precondition restaurantFile != null
	 * @postcondition none
	 * 
	 * @param restaurantFile
	 *            the restaurant menu data file
	 */
	public RestaurantFileReader(File restaurantFile) {
		if (restaurantFile == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.RESTAURANT_FILE_CANNOT_BE_NULL);
		}
		
		this.restaurantFile = restaurantFile;
	}
	
	/**
	 * Opens the associated rmd file and reads all the menu items in the file one
	 * line at a time. Parses each line and creates a menu item object and adds it
	 * to the restaurant. 
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param restaurantGroup the group of restaurants to add menu items to
	 * @throws FileNotFoundException the file not found exception
	 */
	public void loadMenuItemsIntoRestaurant(RestaurantGroup restaurantGroup) throws FileNotFoundException {
		String lastLine = "";
		
		try (Scanner input = new Scanner(this.restaurantFile)) {
			while (input.hasNextLine()) {
				lastLine = input.nextLine();
				String[] fields = this.splitLine(lastLine, RestaurantFileReader.FIELD_SEPARATOR);
				
				restaurantGroup.addRestaurant(fields[0]);
				
				try {
					MenuItem menuItem = this.makeMenuItem(fields);
					restaurantGroup.addMenuItem(fields[0], menuItem);
				} catch (Exception e) {
					String fileName = "readErrors.log";
					FileOutputStream fos = new FileOutputStream(fileName, true);
				    try {
						fos.write(("Error reading file: " + e.getMessage() + ": " + lastLine + System.lineSeparator()).getBytes());
						fos.close();
				    } catch (IOException e2) {
				    	System.err.println(e.getMessage());
				    } 
				}
			}
		}
	}
	
	private MenuItem makeMenuItem(String[] fields) {
		String name = fields[1];
		String description = fields[2];
		int calories = Integer.parseInt(fields[3]);
		double price = Double.parseDouble(fields[4]);
		int rating = Integer.parseInt(fields[5]);
		return new MenuItem(name, description, price, calories, rating);
	}
	
	private String[] splitLine(String line, String fieldSeparator) {
		return line.split(fieldSeparator);
	}

//	/**
//	 * Opens the associated rmd file and reads all the menu items in the file one
//	 * line at a time. Parses each line and creates a menu item object and stores it in
//	 * an ArrayList of menu item objects. Once the file has been completely read the
//	 * ArrayList of menu items is returned from the method. Assumes all
//	 * menu items in the file are for the same restaurant.
//	 * 
//	 * @precondition none
//	 * @postcondition none
//	 * 
//	 * @return Collection of menu item objects read from the file.
//	 * @throws FileNotFoundException the file not found exception
//	 */
//	public ArrayList<MenuItem> loadAllMenuItems() throws FileNotFoundException {
//		ArrayList<MenuItem> menuItems = new ArrayList<MenuItem>();
//		String lastLine = "";
//		
//		try (Scanner input = new Scanner(this.restaurantFile)) {
//			while (input.hasNextLine()) {
//				lastLine = input.nextLine();
//				String[] fields = this.splitLine(lastLine, RestaurantFileReader.FIELD_SEPARATOR);
//				
//				try {
//					MenuItem menuItem = this.makeMenuItem(fields);
//					menuItems.add(menuItem);
//				} catch (Exception e) {
//					System.err.println("Error reading file: " + e.getMessage() + ": " + lastLine);
//				}
//			}
//		}
//		
//		return menuItems;
//		
//	}	

}
