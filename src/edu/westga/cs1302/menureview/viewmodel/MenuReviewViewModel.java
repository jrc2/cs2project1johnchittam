package edu.westga.cs1302.menureview.viewmodel;

import java.io.File;
import java.io.FileNotFoundException;

import edu.westga.cs1302.menureview.datatier.RestaurantFileReader;
import edu.westga.cs1302.menureview.datatier.RestaurantFileWriter;
import edu.westga.cs1302.menureview.model.MenuItem;
import edu.westga.cs1302.menureview.model.Restaurant;
import edu.westga.cs1302.menureview.model.RestaurantGroup;
import edu.westga.cs1302.menureview.model.Validator;
import edu.westga.cs1302.menureview.resources.UI;
import edu.westga.cs1302.menureview.view.output.ReportGenerator;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;

/**
 * The Class MenuReviewViewModel.
 * 
 * @author CS1302
 */
public class MenuReviewViewModel {
	private RestaurantGroup restaurants;
	private ReportGenerator report;
	private ObjectProperty<Restaurant> selectedRestaurantProperty;
	
	private StringProperty nameProperty;
	private StringProperty descriptionProperty;
	private StringProperty caloriesProperty;
	private StringProperty priceProperty;
	private StringProperty ratingProperty;
	
	private StringProperty errorProperty;
	private StringProperty menuErrorProperty;
	private StringProperty nameErrorProperty;
	private StringProperty descriptionErrorProperty;
	private StringProperty caloriesErrorProperty;
	private StringProperty priceErrorProperty;
	private StringProperty ratingErrorProperty;
	
	private StringProperty searchTermProperty;
	private StringProperty minRatingProperty;
	private StringProperty minRatingErrorProperty;
	private StringProperty maxRatingProperty;
	private StringProperty maxRatingErrorProperty;
	
	private ObjectProperty<MenuItem> selectedMenuItemProperty;

	private ListProperty<MenuItem> menuItemsProperty;
	private StringProperty reportProperty;
	
	private ListProperty<Restaurant> restaurantsProperty;
	private StringProperty restaurantNameProperty;
	
	/**
	 * Instantiates a new menu review view model.
	 * 
	 * @precondition none
	 * @postcondition none 
	 */
	public MenuReviewViewModel() {
		this.restaurants = new RestaurantGroup();
		this.report = new ReportGenerator();
		this.selectedRestaurantProperty = new SimpleObjectProperty<Restaurant>();
		
		this.nameProperty = new SimpleStringProperty();
		this.descriptionProperty = new SimpleStringProperty();
		this.caloriesProperty = new SimpleStringProperty();
		this.priceProperty = new SimpleStringProperty();
		this.ratingProperty = new SimpleStringProperty();
		
		this.errorProperty = new SimpleStringProperty();
		this.menuErrorProperty = new SimpleStringProperty();
		this.nameErrorProperty = new SimpleStringProperty();
		this.descriptionErrorProperty = new SimpleStringProperty();
		this.caloriesErrorProperty = new SimpleStringProperty();
		this.priceErrorProperty = new SimpleStringProperty();
		this.ratingErrorProperty = new SimpleStringProperty();
		
		this.searchTermProperty = new SimpleStringProperty();
		this.minRatingProperty = new SimpleStringProperty();
		this.minRatingErrorProperty = new SimpleStringProperty();
		this.maxRatingProperty = new SimpleStringProperty();
		this.maxRatingErrorProperty = new SimpleStringProperty();
		
		this.selectedMenuItemProperty = new SimpleObjectProperty<MenuItem>();

		this.menuItemsProperty = new SimpleListProperty<MenuItem>();
		this.reportProperty = new SimpleStringProperty();
		
		this.restaurantsProperty = new SimpleListProperty<Restaurant>();
		this.restaurantNameProperty = new SimpleStringProperty();
		
		this.resetRestaurants();
	}

	/**
	 * Name property.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the name property
	 */
	public StringProperty nameProperty() {
		return this.nameProperty;
	}

	/**
	 * Description property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the description property
	 */
	public StringProperty descriptionProperty() {
		return this.descriptionProperty;
	}

	/**
	 * calories property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the calories property
	 */
	public StringProperty caloriesProperty() {
		return this.caloriesProperty;
	}

	/**
	 * Price property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the price property
	 */
	public StringProperty priceProperty() {
		return this.priceProperty;
	}
	
	/**
	 * Rating property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the rating property
	 */
	public StringProperty ratingProperty() {
		return this.ratingProperty;
	}
	
	/**
	 * error property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the error property
	 */
	public StringProperty errorProperty() {
		return this.errorProperty;
	}
	
	/**
	 * menu error property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the menu error property
	 */
	public StringProperty menuErrorProperty() {
		return this.menuErrorProperty;
	}
	
	/**
	 * name error property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the name error property
	 */
	public StringProperty nameErrorProperty() {
		return this.nameErrorProperty;
	}
	
	/**
	 * description error property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the description error property
	 */
	public StringProperty descriptionErrorProperty() {
		return this.descriptionErrorProperty;
	}
	
	/**
	 * calories error property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the calories error property
	 */
	public StringProperty caloriesErrorProperty() {
		return this.caloriesErrorProperty;
	}
	
	/**
	 * error price property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the price error property
	 */
	public StringProperty priceErrorProperty() {
		return this.priceErrorProperty;
	}
	
	/**
	 * rating error property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the rating error property
	 */
	public StringProperty ratingErrorProperty() {
		return this.ratingErrorProperty;
	}
	
	/**
	 * search term property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the search term property
	 */
	public StringProperty searchTermProperty() {
		return this.searchTermProperty;
	}
	
	/**
	 * min rating property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the min rating property
	 */
	public StringProperty minRatingProperty() {
		return this.minRatingProperty;
	}
	
	/**
	 * min rating error property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the min rating error property
	 */
	public StringProperty minRatingErrorProperty() {
		return this.minRatingErrorProperty;
	}
	
	/**
	 * max rating property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the max rating property
	 */
	public StringProperty maxRatingProperty() {
		return this.maxRatingProperty;
	}
	
	/**
	 * max rating error property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the max rating error property
	 */
	public StringProperty maxRatingErrorProperty() {
		return this.maxRatingErrorProperty;
	}
	
	/**
	 * selected menu item property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the selected menu item property
	 */
	public ObjectProperty<MenuItem> selectedMenuItemProperty() {
		return this.selectedMenuItemProperty;
	}

	/**
	 * menu items property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the list of menu items property
	 */
	public ListProperty<MenuItem> menuItemsProperty() {
		return this.menuItemsProperty;
	}

	/**
	 * report property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the report property
	 */
	public StringProperty reportProperty() {
		return this.reportProperty;
	}
	
	/**
	 * restaurants property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the list of restaurants property
	 */
	public ListProperty<Restaurant> restaurantsProperty() {
		return this.restaurantsProperty;
	}
	
	/**
	 * Restaurant name property.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the restaurant name property
	 */
	public StringProperty restaurantNameProperty() {
		return this.restaurantNameProperty;
	}
	
	/**
	 * Selected restaurant property.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the selected restaurant property
	 */
	public ObjectProperty<Restaurant> selectedRestaurantProperty() {
		return this.selectedRestaurantProperty;
	}
	
	/**
	 * Adds a menu item with specified attributes to the menu.
	 *
	 * @return true if menu item added successfully, false otherwise
	 */
	public boolean addMenuItem() {
		Restaurant restaurant = this.selectedRestaurantProperty.get();
		Validator validator = new Validator();
		String name = validator.validateName(this.nameProperty.get());
		String description = validator.validateDescription(this.descriptionProperty.get());
		Integer calories = validator.validateCalories(this.caloriesProperty.get());
		Double price = validator.validatePrice(this.priceProperty.get());
		Integer rating = validator.validateRating(this.ratingProperty.get());
		this.nameErrorProperty.set(validator.getNameError());
		this.descriptionErrorProperty.set(validator.getDescriptionError());
		this.caloriesErrorProperty.set(validator.getCaloriesError());
		this.priceErrorProperty.set(validator.getPriceError());
		this.ratingErrorProperty.set(validator.getRatingError());
		
		if (name != null && description != null && calories != null && price != null && rating != null) {
			try {
				MenuItem menuItem = new MenuItem(name, description, price, calories, rating);
				if (restaurant.getMenu().add(menuItem)) {
					this.resetFormFields();
					return true;
				} else {
					this.menuErrorProperty.set(UI.ExceptionMessages.NAME_ALREADY_EXISTS);
				}
			} catch (IllegalArgumentException e) {
				this.menuErrorProperty.set(e.getMessage());
			}
		}

		return false;		
	}

	/**
	 * Update selected menu item with new description, calories, price, and rating
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return true, if successful
	 */
	public boolean updateMenuItem() {
		MenuItem menuItem = this.selectedMenuItemProperty.get();
		if (menuItem == null) {
			return false;
		}
		
		Validator validator = new Validator();
		String description = validator.validateDescription(this.descriptionProperty.get());
		Integer calories = validator.validateCalories(this.caloriesProperty.get());
		Double price = validator.validatePrice(this.priceProperty.get());
		Integer rating = validator.validateRating(this.ratingProperty.get());
		this.nameErrorProperty.set(validator.getNameError());
		this.descriptionErrorProperty.set(validator.getDescriptionError());
		this.caloriesErrorProperty.set(validator.getCaloriesError());
		this.priceErrorProperty.set(validator.getPriceError());
		this.ratingErrorProperty.set(validator.getRatingError());
		
		if (!validator.foundError()) {
			try {
				menuItem.setDescription(description);
				menuItem.setCalories(calories);
				menuItem.setPrice(price);
				menuItem.setRating(rating);
	
				this.resetFormFields(); 
				return true;
			} catch (IllegalArgumentException e) {
				this.menuErrorProperty.set(e.getMessage());
			}
		}
		
		return false;
	}
	
	/**
	 * Delete selected menu item from the menu.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return true, if menu item successfully deleted, false otherwise
	 */
	public boolean deleteMenuItem() {
		Restaurant restaurant = this.selectedRestaurantProperty.get();
		MenuItem menuItem = this.selectedMenuItemProperty.get();
		
		if (restaurant.getMenu().remove(menuItem)) {
			this.resetFormFields(); 
			return true;
		}
		
		return false;
	}
	
	/**
	 * Add restaurant to the restaurant group.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return true, if restaurant successfully added; false otherwise
	 */
	public boolean addRestaurant() {
		String restaurantName = this.restaurantNameProperty.get();
		if (this.restaurants.addRestaurant(restaurantName)) {
			this.resetRestaurants();
			return true;
		}
		return false;
	}
	
	/**
	 * Save the restaurant menu to the specified File.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @param file the file to save the restaurant menu to
	 */
	public void saveRestaurantMenu(File file) {
		RestaurantGroup restaurantGroup = this.restaurants;
		RestaurantFileWriter writer = new RestaurantFileWriter(file);
		try {
			writer.writeMenuItemsForAllRestaurants(restaurantGroup);
		} catch (FileNotFoundException e) {
			this.errorProperty.set(e.getMessage());
		}
	}

	/**
	 * Loads the restaurant menu file using the specified File object.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param file The file to load the restaurant menu from
	 */
	public void loadRestaurantMenu(File file) {
		RestaurantGroup restaurantGroup = this.restaurants;
		RestaurantFileReader reader = new RestaurantFileReader(file);
		try {
			reader.loadMenuItemsIntoRestaurant(restaurantGroup);
			this.resetFormFields();
			this.resetRestaurants();
		} catch (FileNotFoundException e) {
			this.errorProperty.set(e.getMessage());
		} 
	}

	/**
	 * Applies filter values to the selected restaurant
	 * 
	 * @precondition none
	 * @postcondition updated summary report
	 */
	public void applyFiltersToSelectedRestaurant() {
		Restaurant restaurant = this.selectedRestaurantProperty.get();
		String searchTerm = this.searchTermProperty.get();
		
		String minRatingString = this.minRatingProperty.get();
		int minRating = 1;
		if (!minRatingString.isEmpty()) {
			minRating = Integer.parseInt(minRatingString);
		}
		
		String maxRatingString = this.maxRatingProperty.get();
		int maxRating = MenuItem.UPPER_BOUND_RATING;
		if (!maxRatingString.isEmpty()) {
			maxRating = Integer.parseInt(maxRatingString);
		} 

		String results = this.report.searchResults(searchTerm, restaurant, minRating, maxRating);
		this.reportProperty.set(results);
	}
	
	/**
	 * Applies filter values to all restaurants
	 * 
	 * @precondition none
	 * @postcondition updated summary report
	 */
	public void applyFiltersToAllRestaurants() {
		String searchTerm = this.searchTermProperty.get();
		
		String minRatingString = this.minRatingProperty.get();
		int minRating = 1;
		if (!minRatingString.isEmpty()) {
			minRating = Integer.parseInt(minRatingString);
		}
		
		String maxRatingString = this.maxRatingProperty.get();
		int maxRating = MenuItem.UPPER_BOUND_RATING;
		if (!maxRatingString.isEmpty()) {
			maxRating = Integer.parseInt(maxRatingString);
		}
		
		String summaryReport = this.report.findSearchResultsForAllRestaurants(searchTerm, this.restaurants, minRating, maxRating);
		this.reportProperty.set(summaryReport);
	}
	
	/**
	 * Clears all filter values
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void clearFilters() {
		this.searchTermProperty.set("");
		this.minRatingProperty.set("");
		this.minRatingErrorProperty.set("");
		this.maxRatingProperty.set("");
		this.maxRatingErrorProperty.set("");
		this.updateSummary();
	}
	
	private void updateSummary() {
		Restaurant restaurant = this.selectedRestaurantProperty.get();
		String summaryReport = this.report.buildFullSummaryReport(restaurant);
		this.reportProperty.set(summaryReport);
	}

	private void resetRestaurants() {
		this.restaurantNameProperty.set("");
		this.restaurantsProperty.set(FXCollections.observableArrayList(this.restaurants.getRestaurants()));
	}
	
	/**
	 * Resets the from content.
	 *
	 * @precondition none
	 * @postcondition none
	 */
	public void resetFormFields() {
		this.nameProperty.set("");
		this.descriptionProperty.set("");
		this.caloriesProperty.set("");
		this.priceProperty.set("");
		this.ratingProperty.set("");
		
		this.errorProperty.set("");
		this.menuErrorProperty.set("");
		this.nameErrorProperty.set("");
		this.descriptionErrorProperty.set("");
		this.caloriesErrorProperty.set("");
		this.priceErrorProperty.set("");
		this.ratingErrorProperty.set("");
		
		this.updateSummary();
		Restaurant restaurant = this.selectedRestaurantProperty.get();
		if (restaurant != null) {
			this.menuItemsProperty.set(FXCollections.observableArrayList(restaurant.getMenu().getMenuItems()));
		}
		this.clearFilters();
	}
}
