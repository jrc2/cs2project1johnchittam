package edu.westga.cs1302.menureview.resources;

import edu.westga.cs1302.menureview.model.MenuItem;

/**
 * Defines string that will be displayed in UI.
 * 
 * @author CS1302
 */
public class UI {
	
	/**
	 * Defines string messages for exception messages for menu review application.
	 */
	public static final class ExceptionMessages {
		public static final String INVALID_NUMBERDECIMALS = "numberDecimals must be >= 0.";

		public static final String NAME_CANNOT_BE_NULL = "name cannot be null.";
		public static final String NAME_CANNOT_BE_EMPTY = "name cannot be empty.";
		public static final String NAME_ALREADY_EXISTS = "item with this name already exists.";
		public static final String DESCRIPTION_CANNOT_BE_NULL = "description cannot be null.";
		public static final String DESCRIPTION_CANNOT_BE_EMPTY = "description cannot be empty.";
		public static final String INVALID_PRICE = "price must be >= 0.";
		public static final String INVALID_CALORIES = "calories must be >= 0.";
		public static final String INVALID_RATING = "rating must be >= 1 and <= " + Integer.toString(MenuItem.UPPER_BOUND_RATING) + ".";
		public static final String INVALID_DECIMAL = "must be a decimal number >= 0";
		public static final String TOO_MANY_DECIMAL_DIGITS = "at most two decimal digits";
		public static final String REQUIRED = "required";
		public static final String INVALID = "invalid value";
		public static final String INVALID_POSITIVE_INTEGER = "must be a whole number >= 0";
		
		public static final String RESTAURANT_FILE_CANNOT_BE_NULL = "restaurant menu file cannot be null.";
		public static final String MENU_CANNOT_BE_NULL = "menu cannot be null.";
		public static final String MENUITEMS_CANNOT_BE_NULL = "menu items cannot be null.";		
		public static final String MENUITEM_CANNOT_BE_NULL = "menuItem cannot be null.";
		public static final String RESTAURANT_NAME_CANNOT_BE_EMPTY = "restaurantName cannot be empty.";
		public static final String RESTAURANT_NAME_CANNOT_BE_NULL = "restaurantName cannot be null.";
		public static final String RESTAURANT_GROUP_CANNOT_BE_NULL = "restaurantGroup cannot be null.";
		
		public static final String SEARCH_QUERY_CANNOT_BE_NULL = "searchQuery cannot be null.";
	}

	/**
	 * Defines string messages to be used in UI messages.
	 */
	public static final class Text {
		public static final String FILE_OPEN = "File open";
		public static final String FILE_SAVE = "File save";
	}

}
