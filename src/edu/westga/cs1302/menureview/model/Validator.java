package edu.westga.cs1302.menureview.model;

import edu.westga.cs1302.menureview.resources.UI;

/**
 * The Class Validator.
 * 
 * @author CS1302
 */
public class Validator {

	private String nameError;
	private String descriptionError;
	private String priceError;
	private String caloriesError;
	private String ratingError;
	
	/**
	 * Instantiates a new validator.
	 * 
	 * @precondition none
	 * @postcondition getNameError().isEmpty() && getDescriptionError().isEmpty() &&
	 *                getPriceError().isEmpty() && getCaloriesError().isEmpty() &&
	 *                getRatingError().isEmpty()
	 */
	public Validator() {
		this.reset();
	}

	/**
	 * Gets the name error.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the name error 
	 */
	public String getNameError() {
		return this.nameError;
	}

	/**
	 * Gets the description error.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the description error
	 */
	public String getDescriptionError() {
		return this.descriptionError;
	}

	/**
	 * Gets the price error.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the price error
	 */
	public String getPriceError() {
		return this.priceError;
	}

	/**
	 * Gets the calories error.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the calories error 
	 */
	public String getCaloriesError() {
		return this.caloriesError;
	}

	/**
	 * Gets the rating error.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the rating error
	 */
	public String getRatingError() {
		return this.ratingError;
	}

	/**
	 * Found error.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return true, if a preceding call to a validation method detected an error
	 */
	public boolean foundError() {
		return !this.nameError.isEmpty() || !this.descriptionError.isEmpty() || !this.priceError.isEmpty()
				|| !this.caloriesError.isEmpty() || !this.ratingError.isEmpty();
	}

	/**
	 * Validate name. 
	 * Removes trailing and leading spaces from name. Checks if the resulting 
	 * string is a valid MenuItem name and sets a suitable error message.
	 *
	 * @precondition none
	 * @postcondition getNameError().isEmpty() if the passed in name is valid;
	 *                otherwise getNameError() returns an suitable error message
	 * 
	 * @param name the name to be validated
	 * @return name without leading and trailing spaces if it is a valid MenuItem
	 *         name; null otherwise
	 */
	public String validateName(String name) {
		name = name.trim();
		if (name == null || name.isEmpty()) {
			this.nameError = UI.ExceptionMessages.REQUIRED;
			return null;
		} else {
			this.nameError = "";
			return name;
		}
	}
	
	/**
	 * Validate description. 
	 * Removes trailing and leading spaces from description. Checks if the resulting 
	 * string is a valid MenuItem description and sets a suitable error message.
	 *
	 * @precondition none
	 * @postcondition getDescriptionError().isEmpty() if the passed in description
	 *                is valid; otherwise getDescriptionError() returns an suitable
	 *                error message
	 *
	 * @param description the description
	 * @return description without leading and trailing spaces if it is a valid
	 *         MenuItem description; null otherwise
	 */
	public String validateDescription(String description) {
		description = description.trim();
		if (description == null || description.isEmpty()) {
			this.descriptionError = UI.ExceptionMessages.REQUIRED;
			return null;
		} else {
			this.descriptionError = "";
			return description;
		}
	}
	
	/**
	 * Validate price. Removes trailing and leading spaces from price. Checks if the
	 * resulting string represents a valid MenuItem price and sets a suitable error
	 * message. A valid price as to be a number >= 0 and can contain at most two
	 * decimal digits.
	 *
	 * @precondition none
	 * @postcondition getPriceError().isEmpty() if the passed in string represents a
	 *                valid price; otherwise getPriceError() returns an suitable
	 *                error message
	 *
	 * @param priceString the string representing a price
	 * @return the value represented by price after leading and trailing spaces have
	 *         been removed; null if price does not represent a valid price
	 */
	public Double validatePrice(String priceString) {
		Double price = null;
		this.priceError = "";
		priceString = priceString.trim();
		
		if (priceString == null || priceString.isEmpty()) {
			this.priceError = UI.ExceptionMessages.REQUIRED;
			return null;
		}

		if (priceString.matches("\\d*\\.\\d{3,}")) {
			this.priceError = UI.ExceptionMessages.TOO_MANY_DECIMAL_DIGITS;
			return null;
		}
		
		if (!priceString.matches("\\d*(\\.\\d{1,2})?")) {
			this.priceError = UI.ExceptionMessages.INVALID_DECIMAL;
			return null;
		}
		
		try {
			price = Double.parseDouble(priceString);
		} catch (NumberFormatException e) {
			this.priceError = UI.ExceptionMessages.INVALID;
			return null;
		}

		return price;
	}
	
	/**
	 * Validate calories.
	 * Removes trailing and leading spaces from calories. Checks if the resulting
	 * string represents a valid MenuItem calories value and sets a suitable error
	 * message. A valid calories value has to be an integer number >= 0.
	 *
	 * @precondition none
	 * @postcondition getCaloriesError().isEmpty() if the passed in string
	 *                represents a valid calories value; otherwise
	 *                getCaloriesError() returns an suitable error message
	 * 
	 * @param caloriesString the string representing a calories value
	 * @return the value represented by calories after leading and trailing spaces
	 *         have been removed; null if calories does not represent a valid
	 *         calories value
	 */
	public Integer validateCalories(String caloriesString) {
		Integer calories = null;
		this.caloriesError = "";
		caloriesString = caloriesString.trim();

		if (caloriesString == null || caloriesString.isEmpty()) {
			this.caloriesError = UI.ExceptionMessages.REQUIRED;
			return null;
		}
		if (!caloriesString.matches("\\d\\d*")) {
			this.caloriesError = UI.ExceptionMessages.INVALID_POSITIVE_INTEGER;
			return null;
		}

		try {
			calories = Integer.parseInt(caloriesString);
		} catch (NumberFormatException e) {
			this.caloriesError = UI.ExceptionMessages.INVALID;
			return null;
		}

		return calories;
	}

	/**
	 * Validate rating.
	 * Removes trailing and leading spaces from rating. Checks if the resulting
	 * string represents a valid MenuItem rating and sets a suitable error message.
	 * A valid rating value has to be an integer number >= 1 and <= 10.
	 *
	 * @precondition none
	 * @postcondition getRatingError().isEmpty() if the passed in string represents
	 *                a valid rating; otherwise getRatingError() returns an suitable
	 *                error message
	 * 
	 * @param ratingString the string representing a rating
	 * @return the value represented by rating after leading and trailing spaces
	 *         have been removed; null if rating does not represent a valid rating
	 */
	public Integer validateRating(String ratingString) {
		Integer rating = null;
		this.ratingError = "";
		ratingString = ratingString.trim();

		if (ratingString == null || ratingString.isEmpty()) {
			this.ratingError =  UI.ExceptionMessages.REQUIRED;
			return null;
		}
		
		if (!ratingString.matches("\\d\\d*")) {
			this.ratingError = UI.ExceptionMessages.INVALID_POSITIVE_INTEGER;
			return null;
		}

		try {
			rating = Integer.parseInt(ratingString);
		} catch (NumberFormatException e) {
			this.ratingError = UI.ExceptionMessages.INVALID;
			return null;
		}

		if (rating < 1 || rating > 10) {
			this.ratingError = UI.ExceptionMessages.INVALID_RATING;
			return null;
		}

		return rating;
	}
	
	/**
	 * Reset.
	 */
	public void reset() {
		this.nameError = "";
		this.descriptionError = "";
		this.priceError = "";
		this.caloriesError = "";
		this.ratingError = "";
	}
}
