package edu.westga.cs1302.menureview.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import edu.westga.cs1302.menureview.resources.UI;

/**
 * The Class Restaurant.
 * 
 * @author CS1302
 */
public class Restaurant {
	private String name;
	private Menu menu;
	
	/**
	 * Instantiates a new restaurant.
	 * 
	 * @precondition none
	 * @postcondition getName() == "Unknown" && getMenu().size() == 0
	 */
	public Restaurant() {
		this.name = "Unknown";
		this.menu = new Menu();
	}
	
	/**
	 * Instantiates a new restaurant.
	 * 
	 * @precondition name cannot be null or empty
	 * @postcondition getName() == name && getMenu().size() == 0
	 *
	 * @param name the restaurant name
	 */
	public Restaurant(String name) {
		if (name == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.RESTAURANT_NAME_CANNOT_BE_NULL);
		}

		if (name.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.RESTAURANT_NAME_CANNOT_BE_EMPTY);
		}

		this.name = this.formatTitleCase(name.trim());
		this.menu = new Menu();
	}
	
	private String formatTitleCase(String string) {
		ArrayList<String> words = new ArrayList<String>();
		ArrayList<String> formattedWords = new ArrayList<String>();
		
		for (String currentPart : string.split(" {1,}")) {
			words.add(currentPart);
		}
		
		for (int i = 0; i < words.size(); i++) {
			if (i == 0 || !this.isSmallWord(words.get(i))) {
				formattedWords.add(this.capitilizeFirstLetter(words.get(i)));
			} else {
				formattedWords.add(words.get(i).toLowerCase());
			}
		}
		
		return this.convertListOfWordsToString(formattedWords);
	}
	
	private String capitilizeFirstLetter(String word) {
		return word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase();
	}
	
	private String convertListOfWordsToString(ArrayList<String> list) {
		String output = "";
		
		for (String currentWord : list) {
			output += currentWord + " ";
		}
		
		return output.trim();
	}
	
	private boolean isSmallWord(String word) {
		File smallWordsFile = new File("smallWords.txt");
		ArrayList<String> smallWords = new ArrayList<String>();
		
		try (Scanner scanner = new Scanner(smallWordsFile)) {
			while (scanner.hasNext()) {
				smallWords.add(scanner.next());
			}
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
		}
		
		if (smallWords.contains(word.toLowerCase())) {
			return true;
		}
		
		return false;
	}

	/**
	 * Gets the restaurant name.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the restaurant name.
	 * 
	 * @precondition name cannot be null or empty
	 * @postcondition getName() == name
	 *
	 * @param name the restaurant name to set
	 */
	public void setName(String name) {
		if (name == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.RESTAURANT_NAME_CANNOT_BE_NULL);
		}

		if (name.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.RESTAURANT_NAME_CANNOT_BE_EMPTY);
		}

		this.name = name;
	}
	
	/**
	 * Gets the menu.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the menu
	 */
	public Menu getMenu() {
		return this.menu;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.name;
	}

}
