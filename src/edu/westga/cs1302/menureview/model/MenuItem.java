package edu.westga.cs1302.menureview.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import edu.westga.cs1302.menureview.resources.UI;

/**
 * The class MenuItem.
 * 
 * @author CS1302
 * @version Spring 2019
 */
public class MenuItem {
	public static final int UPPER_BOUND_RATING = 10;
	
	private String name;
	private String description;
	private double price;
	private int calories;
	private int rating;

	/**
	 * Instantiates a new menu item.
	 * 
	 * @precondition name != null && name is not empty; description != null &&
	 *               description is not empty; price >= 0; calories >= 0; rating >=
	 *               1 && rating <= UPPER_BOUND_RATING
	 * @postcondition getName() == name && getDescription() == description &&
	 *                getPrice() == price && getCalories() == calories &&
	 *                getRating() == rating
	 * 
	 * @param name        the name
	 * @param description the description
	 * @param price       the price
	 * @param calories    the calories
	 * @param rating      the rating
	 */
	public MenuItem(String name, String description, double price, int calories, int rating) {
		if (name == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NAME_CANNOT_BE_NULL);
		}
		if (name.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NAME_CANNOT_BE_EMPTY);
		}
		if (name.equals("test")) {
			throw new IllegalArgumentException("Test item is not valid");
		}
		if (description == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.DESCRIPTION_CANNOT_BE_NULL);
		}
		if (description.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.DESCRIPTION_CANNOT_BE_EMPTY);
		}
		if (price < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVALID_PRICE);
		}
		if (calories < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVALID_CALORIES);
		}
		if (rating < 1 || rating > UPPER_BOUND_RATING) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVALID_RATING);
		}
		
		this.name = this.formatTitleCase(name.trim());
		this.description = description;
		this.price = price;
		this.calories = calories;
		this.rating = rating;
	}
	
	private String formatTitleCase(String string) {
		ArrayList<String> words = new ArrayList<String>();
		ArrayList<String> formattedWords = new ArrayList<String>();
		
		for (String currentPart : string.split(" {1,}")) {
			words.add(currentPart);
		}
		
		for (int i = 0; i < words.size(); i++) {
			if (i == 0 || !this.isSmallWord(words.get(i))) {
				formattedWords.add(this.capitilizeFirstLetter(words.get(i)));
			} else {
				formattedWords.add(words.get(i).toLowerCase());
			}
		}
		
		return this.convertListOfWordsToString(formattedWords);
	}
	
	private String capitilizeFirstLetter(String word) {
		return word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase();
	}
	
	private String convertListOfWordsToString(ArrayList<String> list) {
		String output = "";
		
		for (String currentWord : list) {
			output += currentWord + " ";
		}
		
		return output.trim();
	}
	
	private boolean isSmallWord(String word) {
		File smallWordsFile = new File("smallWords.txt");
		ArrayList<String> smallWords = new ArrayList<String>();
		
		try (Scanner scanner = new Scanner(smallWordsFile)) {
			while (scanner.hasNext()) {
				smallWords.add(scanner.next());
			}
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
		}
		
		if (smallWords.contains(word.toLowerCase())) {
			return true;
		}
		
		return false;
	}

	/**
	 * Gets the name.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Gets the description.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}
	
	/**
	 * Gets the price.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the price
	 */
	public double getPrice() {
		return this.price;
	}
	
	/**
	 * Gets the calories.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the calories
	 */
	public int getCalories() {
		return this.calories;
	}
	
	/**
	 * Gets the rating.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the rating
	 */
	public int getRating() {
		return this.rating;
	}
	
	/**
	 * Sets the description.
	 *
	 * @precondition description != null && description is not empty
	 * @postcondition getDescription() == description
	 * 
	 * @param description the price to set
	 */
	public void setDescription(String description) {
		if (description == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.DESCRIPTION_CANNOT_BE_NULL);
		}
		if (description.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.DESCRIPTION_CANNOT_BE_EMPTY);
		}
		this.description = description;
	}
	
	/**
	 * Sets the price.
	 *
	 * @precondition price >= 0
	 * @postcondition getPrice() == price
	 * 
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		if (price < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVALID_PRICE);
		}
		this.price = price;
	}
	
	/**
	 * Sets the calories.
	 *
	 * @precondition calories >= 0
	 * @postcondition getCaloriese() == calories
	 * 
	 * @param calories the calories to set
	 */
	public void setCalories(int calories) {
		if (calories < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVALID_CALORIES);
		}
		this.calories = calories;
	}
	
	/**
	 * Sets the rating.
	 *
	 * @precondition rating >= 1 && rating <= UPPER_BOUND_RATING
	 * @postcondition getRating() == rating
	 * 
	 * @param rating the rating to set
	 */
	public void setRating(int rating) {
		if (rating < 1 || rating > UPPER_BOUND_RATING) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVALID_RATING);
		}
		this.rating = rating;
	}

	@Override
	public String toString() {
		return this.name + ": " + this.description;
	}
}
