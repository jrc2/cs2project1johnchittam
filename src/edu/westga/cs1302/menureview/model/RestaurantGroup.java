package edu.westga.cs1302.menureview.model;

import java.util.ArrayList;

import edu.westga.cs1302.menureview.resources.UI;


/**
 * The Class RestaurantGroup - store a collection of restaurants
 * 
 * @author CS1302
 */
public class RestaurantGroup {
	private ArrayList<Restaurant> restaurants;

	/**
	 * Instantiates a new restaurant group.
	 */
	public RestaurantGroup() {
		this.restaurants = new ArrayList<Restaurant>();
	}

	/**
	 * Adds the restaurant.
	 * 
	 * @precondition name not null && name not empty
	 * @postcondition if new restaurant then adds new restaurant 
	 *
	 * @param name the name of the new restaurant
	 * @return true, if adding new restaurant; false, if restaurant already exists
	 */
	public boolean addRestaurant(String name) {
		if (name == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NAME_CANNOT_BE_NULL);
		}
		
		if (name.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NAME_CANNOT_BE_EMPTY);
		}

		for (Restaurant currRestaurant : this.restaurants) {
			if (name.equalsIgnoreCase(currRestaurant.getName())) {
				return false;
			}
		}

		Restaurant restaurant = new Restaurant(name);
		return this.restaurants.add(restaurant);
	}

	/**
	 * Finds restaurant with specified name
	 *
	 * @param name the name 
	 * @return the restaurant if found; null otherwise
	 */
	public Restaurant findRestaurant(String name) {
		if (name == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NAME_CANNOT_BE_NULL);
		}

		for (Restaurant currRestaurant : this.restaurants) {
			if (name.equalsIgnoreCase(currRestaurant.getName())) {
				return currRestaurant;
			}
		}

		return null;
	}

	/**
	 * Adds the specified menu item to the specified restaurant
	 * 
	 * @precondition restaurantName not null && menuItem not null
	 * @postconditon menuItem added to specified restaurant
	 *
	 * @param restaurantName the restaurant name
	 * @param menuItem the menu item
	 * @return true, if menu item successfully added; false, otherwise
	 */
	public boolean addMenuItem(String restaurantName, MenuItem menuItem) {
		if (restaurantName == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NAME_CANNOT_BE_NULL);
		}

		if (menuItem == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.MENUITEM_CANNOT_BE_NULL);
		}
		
		Restaurant restaurant = this.findRestaurant(restaurantName);
		if (restaurant != null) {
			return restaurant.getMenu().add(menuItem);
		}

		return false;
	}

	/**
	 * Removes the specified menu item from the specified restaurant name.
	 * 
	 * @precondition restaurantName != null && menuItem != null
	 * @postcondition if menuItem found at restaurant, the menu size for that restaurant
	 *               is reduced by one
	 *
	 * @param restaurantName the restaurant name
	 * @param menuItem the menu item
	 * @return true, if successful
	 */
	public boolean removeMenuItem(String restaurantName, MenuItem menuItem) {
		if (restaurantName == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NAME_CANNOT_BE_NULL);
		}

		if (menuItem == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.MENUITEM_CANNOT_BE_NULL);
		}

		Restaurant restaurant = this.findRestaurant(restaurantName);
		if (restaurant != null) {
			return restaurant.getMenu().remove(menuItem);
		}

		return false;
	}

	/**
	 * Gets the restaurants.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the restaurants
	 */
	public ArrayList<Restaurant> getRestaurants() {
		return this.restaurants;
	}
}
