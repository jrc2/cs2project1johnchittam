package edu.westga.cs1302.menureview.model;

import java.util.ArrayList;
import edu.westga.cs1302.menureview.resources.UI;

/**
 * The Class Menu.
 * 
 * @author CS1302
 */
public class Menu {
	private ArrayList<MenuItem> menuItems;

	/**
	 * Instantiates a new menu.
	 * 
	 * @precondition none
	 * @postcondition size() == 0
	 */
	public Menu() {
		this.menuItems = new ArrayList<MenuItem>();
	}

	/**
	 * Numbers of menu items on the menu.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the number of menu items on the menu.
	 */
	public int size() {
		return this.menuItems.size();
	}

	/**
	 * Adds the menu item to the menu if a matching item does not already exist.
	 * 
	 * @precondition menuItem != null
	 * @postcondition size() == size()@prev + 1
	 *
	 * @param menuItem the menu item
	 * @return true, if add successful, false otherwise
	 */
	public boolean add(MenuItem menuItem) {
		if (menuItem == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.MENUITEM_CANNOT_BE_NULL);
		}
		
		for (MenuItem currItem : this.menuItems) {
			if (menuItem.getName().trim().toLowerCase().equals(currItem.getName().toLowerCase())) {
				return false;
			}
		}

		return this.menuItems.add(menuItem);
	}

	/**
	 * Adds all the menu items to the menu.
	 * 
	 * @precondition menuItems != null
	 * @postcondition size() == size()@prev + menuItems.size()
	 *
	 * @param menuItems the menu items to add to the menu
	 * 
	 * @return true, if add successful
	 */
	public boolean addAll(ArrayList<MenuItem> menuItems) {
		if (menuItems == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.MENUITEMS_CANNOT_BE_NULL);
		}

		return this.menuItems.addAll(menuItems);
	}
	
	/**
	 * Deletes the specified menu Item from the menu.
	 * 
	 * @precondition none
	 * @postcondition if found, size() == size()@prev � 1
	 * 
	 * @param menuItem the menu item to delete from the menu.
	 * 
	 * @return true if the menu item was found and deleted from the menu, false
	 *         otherwise
	 */
	public boolean remove(MenuItem menuItem) {
		return this.menuItems.remove(menuItem);
	}

	/**
	 * Gets the menu items.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the autos
	 */
	public ArrayList<MenuItem> getMenuItems() {
		return this.menuItems;
	}

	/**
	 * Find the price of the most expensive item.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the price of the most expensive menu item or a negative value if no
	 *         menu items
	 */
	public double findPriceOfMostExpensiveItem() {
		double highest = -1;

		for (MenuItem item : this.menuItems) {
			if (item.getPrice() > highest) {
				highest = item.getPrice();
			}
		}

		return highest;
	}
	
	/**
	 * Find the name of a menu item with the highest rating.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the name of an menu item with the highest rating or null if no menu
	 *         items
	 */
	public String findNameOfBestMenuItem() {
		int bestRating = 0;
		String name = null; 

		for (MenuItem item : this.menuItems) {
			if (item.getRating() > bestRating) {
				bestRating = item.getRating();
				name = item.getName();
			}
		}
		return name;
	}
	
	/**
	 * Find the average price of all menu items with the specified rating.
	 *
	 * @precondition rating >=1 && rating <= MenuItem.UPPER_BOUND_RATING
	 * @postcondition none
	 * 
	 * @param rating  the rating of the menu items to be averaged
	 * @return the average price of all menu items with the specified rating
	 */
	public double findAveragePriceForRating(int rating) {
		if (rating < 1 || rating > MenuItem.UPPER_BOUND_RATING) {
			throw new IllegalArgumentException("invalid rating");
		}
		
		int count = 0;
		double sum = 0;
		for (MenuItem item : this.menuItems) {
			if (item.getRating() == rating) {
				count++;
				sum += item.getPrice();
			}
		}
		if (count == 0) {
			return 0;
		}
		return sum / count;
	}
	
	
	/**
	 * Find the menu item with highest rating.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the menu item with highest or null if no menu items
	 */
	public MenuItem findBestMenuItem() {
		int highest = -1;
		MenuItem item = null;

		for (MenuItem currItem : this.menuItems) {
			if (currItem.getRating() > highest) {
				highest = currItem.getRating();
				item = currItem;
			}
		}

		return item;
	}
	
	/**
	 * Find the menu item with the lowest rating.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the menu item with the lowest rating or null if no menu items
	 */
	public MenuItem findWorstMenuItem() {
		int lowest = MenuItem.UPPER_BOUND_RATING + 1;
		MenuItem item = null;

		for (MenuItem currItem : this.menuItems) {
			if (currItem.getRating() < lowest) {
				lowest = currItem.getRating();
				item = currItem;
			}
		}

		return item;
	}
	
	/**
	 * Counts the menu items by rating.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return array where the value at index i is the number of menu items with rating i+1
	 */
	public int[] getNumberItemsByRating() {
		int[] count = new int[MenuItem.UPPER_BOUND_RATING];

		for (MenuItem currItem : this.menuItems) {
			count[currItem.getRating() - 1]++;
		}

		return count;
	}
	
	/**
	 * Searches for the searchTerm in a Menu's item names and descriptions (not case 
	 * sensitive) and returns a list of all items that have a match in the name or description
	 * and have a rating within the min and max rating provided.
	 * 
	 * @precondition searchTerm!=null AND minRating>=1 AND maxRating<=MenuItem.UPPER_BOUND_RATING
	 * @postcondition none
	 * 
	 * @param searchTerm the term to search for
	 * @param minRating the minimum rating results can have
	 * @param maxRating the maximum rating results can have
	 * @return a list of matched items
	 */
	public ArrayList<MenuItem> filterItems(String searchTerm, int minRating, int maxRating) {
		if (searchTerm == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.SEARCH_QUERY_CANNOT_BE_NULL);
		}
		if (minRating < 1 || maxRating > MenuItem.UPPER_BOUND_RATING) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVALID_RATING);
		}
		
		ArrayList<MenuItem> matchedItems = new ArrayList<MenuItem>();
		String trimmedLowercaseQuery = searchTerm.trim().toLowerCase();
		
		for (MenuItem currentItem : this.menuItems) {
			String lowercaseName = currentItem.getName().toLowerCase();
			String lowercaseDescription = currentItem.getDescription().toLowerCase();
			int rating = currentItem.getRating();
			
			if ((lowercaseName.contains(trimmedLowercaseQuery) 
					|| lowercaseDescription.contains(trimmedLowercaseQuery)) 
					&& rating >= minRating && rating <= maxRating) {
				matchedItems.add(currentItem);
			}
		}
		
		return matchedItems;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Menu with " + this.size() + " items";
	}
}
