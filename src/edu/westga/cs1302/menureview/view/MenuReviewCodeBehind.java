package edu.westga.cs1302.menureview.view;

import java.io.File;

import edu.westga.cs1302.menureview.model.MenuItem;
import edu.westga.cs1302.menureview.model.Restaurant;
import edu.westga.cs1302.menureview.viewmodel.MenuReviewViewModel;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 * MenuReviewCodeBehind defines the "controller" for MenuReviewGui.fxml.
 * 
 * @author CS1302
 */
public class MenuReviewCodeBehind {
	private MenuReviewViewModel theViewModel;
	private ObjectProperty<MenuItem> selectedMenuItem;
	private ObjectProperty<Restaurant> selectedRestaurant;
	
	@FXML
	private AnchorPane pane;

	@FXML
	private TextField nameTextField;

	@FXML
	private TextArea descriptionTextArea;

	@FXML
	private TextField caloriesTextField;

	@FXML
	private TextField priceTextField;
	
	@FXML
	private TextField ratingTextField;

	@FXML
	private Label errorLabel;
	
	@FXML
	private Label menuErrorLabel;
	
	@FXML
	private Label nameErrorLabel;
	
	@FXML
	private Label descriptionErrorLabel;
	
	@FXML
	private Label caloriesErrorLabel;
	
	@FXML
	private Label priceErrorLabel;
	
	@FXML
	private Label ratingErrorLabel;

	@FXML
	private Button addButton;

	@FXML
	private Button updateButton;

	@FXML
	private ListView<MenuItem> menuItemsListView;

	@FXML
	private Button deleteButton;

	@FXML
	private TextArea summaryTextArea;
	
	@FXML
	private ComboBox<Restaurant> restaurantsComboBox;

	@FXML
	private TextField restaurantNameTextField;
	
	@FXML
	private Button addRestaurantButton;
	
	@FXML
	private Pane menuPane;
	
	@FXML
	private Pane summaryPane;

	@FXML
	private TextField searchTermTextField;
	
	@FXML
	private TextField minRatingTextField;
	
	@FXML
	private Label minRatingErrorLabel;
	
	@FXML
	private TextField maxRatingTextField;
	
	@FXML
	private Label maxRatingErrorLabel;
	
	@FXML
	private CheckBox allRestaurantsCheckBox;
	
	/**
	 * Instantiates a new menu review code behind.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public MenuReviewCodeBehind() {
		this.theViewModel = new MenuReviewViewModel();
		this.selectedMenuItem = new SimpleObjectProperty<MenuItem>();
		this.selectedRestaurant = new SimpleObjectProperty<Restaurant>();
	}

	/**
	 * Initializes the GUI components, binding them to the view model properties
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	@FXML
	public void initialize() {
		this.bindComponentsToViewModel();
		this.bindButtonsDisableProperty();
		this.setupListenerForListView();
		this.setupListenerForRestaurantsComboBox();
		
		this.initializeUI();
	}

	private void initializeUI() {
		this.selectedMenuItem.set(null);
	}

	private void setupListenerForListView() {
		this.menuItemsListView.getSelectionModel().selectedItemProperty().addListener((observable, oldMenuItem, newMenuItem) -> {
			if (newMenuItem != null) {
				this.nameTextField.textProperty().set(newMenuItem.getName());
				this.descriptionTextArea.textProperty().set(newMenuItem.getDescription());

				Integer calories = newMenuItem.getCalories();
				this.caloriesTextField.textProperty().set(calories.toString());

				Double price = newMenuItem.getPrice();
				this.priceTextField.textProperty().set(price.toString());
				
				Integer rating = newMenuItem.getRating();
				this.ratingTextField.textProperty().set(rating.toString());

				this.selectedMenuItem.set(newMenuItem);
			}
		});
	}
	
	private void setupListenerForRestaurantsComboBox() {
		this.restaurantsComboBox.getSelectionModel().selectedItemProperty()
				.addListener((observable, oldRestaurant, newRestaurant) -> {
					if (newRestaurant != null) {
						this.selectedRestaurant.set(newRestaurant);
						this.menuPane.disableProperty().set(false);
						this.summaryPane.disableProperty().set(false);
					} else if (oldRestaurant == null) {
						this.menuPane.disableProperty().set(true);
						this.summaryPane.disableProperty().set(true);
					}
					this.theViewModel.resetFormFields();
				});
	}
	
	private void bindButtonsDisableProperty() {
		BooleanBinding menuItemUnselectedBinding = Bindings
				.isNull(this.menuItemsListView.getSelectionModel().selectedItemProperty());
		BooleanBinding emptyFormFieldsBinding = Bindings.isEmpty(this.nameTextField.textProperty())
				.or(this.descriptionTextArea.textProperty().isEmpty())
				.or(this.caloriesTextField.textProperty().isEmpty())
				.or(this.ratingTextField.textProperty().isEmpty())
				.or(this.priceTextField.textProperty().isEmpty());

		this.deleteButton.disableProperty().bind(menuItemUnselectedBinding);
		this.updateButton.disableProperty().bind(menuItemUnselectedBinding.or(emptyFormFieldsBinding));

		this.addButton.disableProperty().bind(emptyFormFieldsBinding);
		this.addRestaurantButton.disableProperty().bind(this.restaurantNameTextField.textProperty().isEmpty());
	}

	private void bindComponentsToViewModel() {
		this.nameTextField.textProperty().bindBidirectional(this.theViewModel.nameProperty());
		this.descriptionTextArea.textProperty().bindBidirectional(this.theViewModel.descriptionProperty());
		this.caloriesTextField.textProperty().bindBidirectional(this.theViewModel.caloriesProperty());
		this.priceTextField.textProperty().bindBidirectional(this.theViewModel.priceProperty());
		this.ratingTextField.textProperty().bindBidirectional(this.theViewModel.ratingProperty());
		
		this.errorLabel.textProperty().bindBidirectional(this.theViewModel.errorProperty());
		this.menuErrorLabel.textProperty().bindBidirectional(this.theViewModel.menuErrorProperty());
		this.nameErrorLabel.textProperty().bindBidirectional(this.theViewModel.nameErrorProperty());
		this.descriptionErrorLabel.textProperty().bindBidirectional(this.theViewModel.descriptionErrorProperty());
		this.caloriesErrorLabel.textProperty().bindBidirectional(this.theViewModel.caloriesErrorProperty());
		this.priceErrorLabel.textProperty().bindBidirectional(this.theViewModel.priceErrorProperty());
		this.ratingErrorLabel.textProperty().bindBidirectional(this.theViewModel.ratingErrorProperty());
		
		this.selectedMenuItem.bindBidirectional(this.theViewModel.selectedMenuItemProperty());

		this.menuItemsListView.itemsProperty().bind(this.theViewModel.menuItemsProperty());
		this.summaryTextArea.textProperty().bind(this.theViewModel.reportProperty());

		this.restaurantsComboBox.itemsProperty().bind(this.theViewModel.restaurantsProperty());
		this.restaurantNameTextField.textProperty().bindBidirectional(this.theViewModel.restaurantNameProperty());
		this.theViewModel.selectedRestaurantProperty().bind(this.selectedRestaurant);
		
		this.searchTermTextField.textProperty().bindBidirectional(this.theViewModel.searchTermProperty());
		this.minRatingTextField.textProperty().bindBidirectional(this.theViewModel.minRatingProperty());
		this.maxRatingTextField.textProperty().bindBidirectional(this.theViewModel.maxRatingProperty());
		this.minRatingErrorLabel.textProperty().bindBidirectional(this.theViewModel.minRatingErrorProperty());
		this.maxRatingErrorLabel.textProperty().bindBidirectional(this.theViewModel.maxRatingErrorProperty());
	}

	@FXML
	private void onAddMenuItem() {
		this.theViewModel.addMenuItem();
	}

	@FXML
	private void onUpdateMenuItem() {
		this.theViewModel.updateMenuItem();
	}

	@FXML
	private void onDeleteMenuItem() {
		this.theViewModel.deleteMenuItem();
	}
	
	@FXML
	private void onAddRestaurant() {
		this.theViewModel.addRestaurant();
	}
	
	@FXML
	private void onApplyFilters() {
		if (this.allRestaurantsCheckBox.isSelected()) {
			this.theViewModel.applyFiltersToAllRestaurants();
		} else {
			this.theViewModel.applyFiltersToSelectedRestaurant();
		}
	}
	
	@FXML
	private void onClearFilters() {
		this.theViewModel.clearFilters();
	}
	
	
    @FXML
    private void onFileOpen() {
		FileChooser fileChooser = new FileChooser();
		this.setFileFilters(fileChooser);

		Window owner = this.pane.getScene().getWindow();
		File selectedFile = fileChooser.showOpenDialog(owner);

		if (selectedFile != null) {
			this.theViewModel.loadRestaurantMenu(selectedFile);
		}
    }

    @FXML
    private void onFileSave() {
		FileChooser fileChooser = new FileChooser();
		this.setFileFilters(fileChooser);

		Window owner = this.pane.getScene().getWindow();
		File selectedFile = fileChooser.showSaveDialog(owner);

		if (selectedFile != null) {
			this.theViewModel.saveRestaurantMenu(selectedFile);
		}
    }
    
	private void setFileFilters(FileChooser fileChooser) {
		ExtensionFilter filter = new ExtensionFilter("Restaurant Menu Data", "*.rmd");
		fileChooser.getExtensionFilters().add(filter);
		filter = new ExtensionFilter("All Files", "*.*");
		fileChooser.getExtensionFilters().add(filter);
	}
}
