package edu.westga.cs1302.menureview.view.output;

import edu.westga.cs1302.menureview.model.MenuItem;
import edu.westga.cs1302.menureview.model.Restaurant;
import edu.westga.cs1302.menureview.model.RestaurantGroup;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import edu.westga.cs1302.menureview.model.Menu;

/**
 * The Class ReportGenerator.
 * 
 * @author CS1302
 */
public class ReportGenerator {

	private static final String SEARCH_RESULT_PARSER_WITH_RATING_BETWEEN = "with rating between ";
	private static final String SEARCH_RESULT_PARSER_AND = " and ";
	private static final String SEARCH_RESULT_PARSE_INCLUSIVE = " inclusive";

	/**
	 * Builds the full summary report of the specified menu of a restaurant. If menu
	 * is null, instead of throwing an exception will return a string saying "No
	 * menu exists.", otherwise builds a summary report of the menu.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param restaurant the restaurant
	 *
	 * @return A formatted summary string of the restaurant's menu.
	 */
	public String buildFullSummaryReport(Restaurant restaurant) {
		String summary = "";

		if (restaurant == null || restaurant.getMenu() == null) {
			summary = "No menu exists.";
		} else {
			Menu menu = restaurant.getMenu();
			summary = restaurant.getName() + System.lineSeparator();
			summary += "#Menu items: " + menu.size() + System.lineSeparator();

			if (menu.size() > 0) {
				MenuItem bestItem = menu.findBestMenuItem();
				MenuItem worstItem = menu.findWorstMenuItem();

				summary += System.lineSeparator();

				summary += "Highest rated item: ";
				summary += this.buildMenuItemOutput(bestItem) + System.lineSeparator();
				summary += "Lowest rated item: ";
				summary += this.buildMenuItemOutput(worstItem) + System.lineSeparator();

				summary += System.lineSeparator();
				summary += this.buildCountsByRating(restaurant);
			}
		}
		return summary;
	}

	private String buildMenuItemOutput(MenuItem menuItem) {
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(Locale.US);
		String output = menuItem.getName() + " " + menuItem.getCalories() + " "
				+ currencyFormatter.format(menuItem.getPrice()) + " " + menuItem.getRating();
		return output;
	}

	private String buildCountsByRating(Restaurant restaurant) {
		Menu menu = restaurant.getMenu();
		int[] count = menu.getNumberItemsByRating();
		String output = "";

		for (int i = 0; i < MenuItem.UPPER_BOUND_RATING; i += 2) {
			output += "# items rated " + (i + 1) + "/" + (i + 2) + ": " + this.barNumberOutput(count[i] + count[i + 1])
					+ System.lineSeparator();
		}

		return output;
	}

	private String barNumberOutput(int numberOfBars) {
		String output = "";

		for (int i = 1; i <= numberOfBars; i++) {
			output += "| ";
		}

		return output;
	}

	/**
	 * Returns a list of all menu items at a restaurant with the trimmed search term
	 * in the name or description
	 * 
	 * @param searchTerm the term to trim and search for
	 * @param restaurant the restaurant to search for items in
	 * @param minRating  the minimum rating results can have
	 * @param maxRating  the maximum rating results can have
	 * @return a String of all menu items that contain the trimmed search term in
	 *         their name or description
	 */
	public String searchResults(String searchTerm, Restaurant restaurant, int minRating, int maxRating) {
		String trimmedSearch = searchTerm.trim();
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(Locale.US);
		Menu menu = restaurant.getMenu();
		String output = "";

		ArrayList<MenuItem> results = menu.filterItems(trimmedSearch, minRating, maxRating);

		output += this.parseSearchResults(results, trimmedSearch, restaurant.getName(), minRating, maxRating);

		if (results.size() > 0) {
			output += System.lineSeparator() + System.lineSeparator() + "Average price of matching items:"
					+ System.lineSeparator() + currencyFormatter.format(this.findAveragePrice(results));
		}

		return output;
	}

	private double findAveragePrice(ArrayList<MenuItem> menuItems) {
		double sum = 0;

		for (MenuItem currItem : menuItems) {
			sum += currItem.getPrice();
		}

		double average = sum / menuItems.size();

		return average;
	}

	/**
	 * Displays search results for menu items at all restaurants.
	 * 
	 * @param searchTerm      the term to trim and search for
	 * @param restaurantGroup the group of restaurants to search through
	 * @param minRating       the minimum rating results can have
	 * @param maxRating       the maximum rating results can have
	 * @return a String of all menu items that contain the trimmed search term in
	 *         their name or description
	 */
	public String findSearchResultsForAllRestaurants(String searchTerm, RestaurantGroup restaurantGroup, int minRating,
			int maxRating) {
		String output = "";
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(Locale.US);
		ArrayList<MenuItem> combinedResults = new ArrayList<MenuItem>();

		for (Restaurant currRestaurant : restaurantGroup.getRestaurants()) {
			String trimmedSearch = searchTerm.trim();
			Menu menu = currRestaurant.getMenu();

			ArrayList<MenuItem> results = menu.filterItems(trimmedSearch, minRating, maxRating);
			combinedResults.addAll(results);

			output += this.parseSearchResults(results, trimmedSearch, currRestaurant.getName(), minRating, maxRating)
					+ System.lineSeparator() + System.lineSeparator();
		}

		output += "Average price of all matching items: "
				+ currencyFormatter.format(this.findAveragePrice(combinedResults));

		return output;
	}

	private String parseSearchResults(ArrayList<MenuItem> results, String trimmedSearch, String restaurantName,
			int minRating, int maxRating) {
		String output = "";
		String queryIndicator = "";
		if (!trimmedSearch.isEmpty()) {
			queryIndicator = " containing " + trimmedSearch;
		}

		if (results.size() == 0) {
			output += "No items" + queryIndicator + " at " + restaurantName + System.lineSeparator()
					+ SEARCH_RESULT_PARSER_WITH_RATING_BETWEEN + minRating + SEARCH_RESULT_PARSER_AND + maxRating
					+ SEARCH_RESULT_PARSE_INCLUSIVE + ".";
		} else {
			output += "Menu items at " + restaurantName + queryIndicator + System.lineSeparator()
					+ SEARCH_RESULT_PARSER_WITH_RATING_BETWEEN + minRating + SEARCH_RESULT_PARSER_AND + maxRating
					+ SEARCH_RESULT_PARSE_INCLUSIVE + ":";
			for (MenuItem currItem : results) {
				output += System.lineSeparator() + currItem.getName();
			}
		}

		return output;
	}
}
